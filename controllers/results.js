const express=require("express")

function add(req,res,next){
    const n1=req.params.n1;
    const n2=req.params.n2;
    res.send(n1+"+"+n2+': '+(parseInt(n1)+parseInt(n2)));
}

function square(req,res,next){
    const n1=req.body.n1;
    const n2=req.body.n2;
    res.send(n1+"^"+n2+': '+ Math.pow(n1,n2));
}

function sub(req,res,next){
    const n1=req.params.n1;
    const n2=req.params.n2;
    res.send(n1+"-"+n2+': '+(parseint(n1)+parseInt(n2)));
}

function divide(req,res,next){
    const n1=req.body.n1;
    const n2=req.body.n2;
    res.send(n1+"/"+n2+': '+(n1/n2));
}
function multiply(req,res,next){
    const n1=req.body.n1;
    const n2=req.body.n2;
    res.send(n1+"*"+n2+': '+(n1*n2));
}
module.exports={add,square,sub,divide,multiply};