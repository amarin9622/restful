var express = require('express');
var router = express.Router();
const controller = require("../controllers/results");
router.get('/:n1/:n2', controller.add);
router.post('/:n1/:n2', controller.multiply);
router.put('/:n1/:n2', controller.divide);
router.patch('/:n1/:n2', controller.square);
router.delete('/:n1/:n2', controller.sub);

module.exports = router;